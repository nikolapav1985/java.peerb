import javax.swing.JOptionPane;

/**
* Quiz class
*
* A quiz example. One reply is correct and makes quiz attempt successful.
* All other replies (single letter) are incorrect (these cause a quiz to continue).
* All other replies having length more than one letter are incorrect and invalid.
*
* compile example ----- javac Quiz.java
*
* run example ------ java Quiz
*
*/
class Quiz{
    public static void main(String[] args){
        String question = "Best method for data input?\n";
        String reply = "";
        String correct = "A";
        String valid = "BCDE";
        question += "A keyboard, \n";
        question += "B mouse, \n";
        question += "C gesture, \n";
        question += "D voice, \n";
        question += "E stylus";

        for(;1==1;){ // keep looping until reply correct
            reply = JOptionPane.showInputDialog(question);
            reply = reply.toUpperCase();
            if(reply.equals(correct)){ // correct reply, break loop
                JOptionPane.showMessageDialog(null, "Correct!");
                break;
            }

            if(reply.length() == 1 && valid.contains(reply)){ // valid reply but not correct
                JOptionPane.showMessageDialog(null, "Incorrect, please try again.");
            } else { // invalid reply
                JOptionPane.showMessageDialog(null, "Ivalid, please enter A, B, C, D or E.");
            }
        }
    }
}
